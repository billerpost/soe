// Access the AMQP callback-based API
var amqp = require('amqplib/callback_api');
var amqpConn = null;
var pubChannel = null;

function publish(data) {
    startConnection();
    createChannel();
    publishMessage(data);
}

function startConnection() {
    amqp.connect(process.env.CLOUDAMQP_URL + "?heartbeat=60", function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(start, 1000);
        }
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 1000);
        });
        console.log("[AMQP] connected");
        amqpConn = conn;
    });
}

function createChannel() {
    amqpConn.createConfirmChannel(function (err, ch) {
        if (closeOnErr(err)) return;
        ch.on("error", function (err) {
            console.error("[AMQP] channel error", err.message);
        });
        ch.on("close", function () {
            console.log("[AMQP] channel closed");
        });
        console.log("[AMQP] channel created");
        pubChannel = ch;
    });
}

function publishMessage(data) {
    pubChannel.assertQueue("EmailResubmitQueue", { durable: true }, function (err, _ok) {
        if (closeOnErr(err)) return;
        pubChannel.sendToQueue("EmailResubmitQueue", Buffer.from(JSON.stringify(data)));
    });
}

module.exports = new publish(data);
