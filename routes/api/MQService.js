// Access the AMQP callback-based API
var amqp = require('amqplib/callback_api');

var publish = function (data) {
    amqp.connect(process.env.CLOUDAMQP_URL + "?heartbeat=60", function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(publish, 1000);
        }
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(publish, 1000);
        });
        console.log("[AMQP] connected");
        // Create Channel to send message
        conn.createChannel(function (err, ch) {
            ch.on("error", function (err) {
                console.error("[AMQP] channel error", err.message);
                return;
            });
            ch.on("close", function () {
                console.log("[AMQP] channel closed");
                return;
            });
            console.log("[AMQP] channel created");

            let queueName = "EmailAttachmentQueue";       // Set queueName based on Action
            // Send message to Queue
            ch.assertQueue(queueName, { durable: true }, function (err, _ok) {
                if (err) {
                    console.error("[AMQP] queue error", err.message);
                }
                ch.sendToQueue(queueName, Buffer.from(JSON.stringify("data")));
                console.log("message published to queue");
            });
        });
    });
}

module.exports = new publish();