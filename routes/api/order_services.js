'use strict';

const orderServices = {
  _orderMaster: [
    {
      uid: 'A123456',
    },
    {
      uid: 'B987654',
    } 
  ],
  getUidInfo: function (uid, callback) {
    console.log("Uid received: " + uid);
    for (let i = 0; i < this._orderMaster.length; i++) {
      console.log("this._orderMaster[i].uid: " + this._orderMaster[i].uid);
      if (this._orderMaster[i].uid === uid) {
        console.log("calling back with order having id: " + this._orderMaster[i].uid);
        callback(null, this._orderMaster[i]);
        return;
      }
    }
    callback(null, null);
  },
};

module.exports = orderServices;
